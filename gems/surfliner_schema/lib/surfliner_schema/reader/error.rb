# frozen_string_literal: true

require "surfliner_schema/reader/error/duplicate_property"
require "surfliner_schema/reader/error/not_recognized"
require "surfliner_schema/reader/error/undefined_schema"

module SurflinerSchema
  module Reader
    module Error
    end
  end
end
