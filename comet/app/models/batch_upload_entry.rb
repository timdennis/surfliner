# frozen_string_literal: true

class BatchUploadEntry < ApplicationRecord
  self.table_name = "batch_upload_entries"
end
