# frozen_string_literal: true

require "csv"

##
# A base module to resolve requests for batch uploads.
module BatchUploadsControllerBehavior
  FILE_NAME_KEY = "file name"

  private

  ##
  # @api private
  #
  # Wrap up files from local mounted staging area for upload
  class StagingAreaFile
    attr_reader :path, :filename, :file_use, :original_filename

    def initialize(path:, filename:, file_use: :original_file)
      @path = path
      @filename = filename
      @original_filename = filename
      @file_use = file_use
    end

    # Content type of a file
    def content_type
      Rack::Mime.mime_type(File.extname(filename)) || "binary/octet-stream"
    end

    ##
    # Create file object for S3/Shrine upload
    def io_file
      File.open(File.join(@path, @filename))
    end
  end

  ##
  # @api private
  #
  # Wrap up files from S3/Minio staging area for upload
  class S3StagingAreaFile < StagingAreaFile
    attr_reader :s3_handler

    ##
    # @param path [String] - the prefix to the file like my-project/
    # @param filename [String] - the filename
    # @param s3_handler [StagingAreaS3Handler]
    def initialize(path:, filename:, s3_handler:, file_use: :original_file)
      super(path: path, filename: filename, file_use: file_use)

      @s3_handler = s3_handler
    end

    ##
    # Create IO-like Down::ChunkedIO object for S3/Shrine upload
    def io_file
      s3_handler = Rails.application.config.staging_area_s3_handler
      Down.download s3_handler.file_url("#{path}#{filename}")
    end
  end

  ##
  # Build and ingest metadata and file()s of an object
  def ingest_object(attrs, file_path)
    process_attrs attrs

    attrs[:depositor] = current_user.email

    perform_ingest(attrs, current_user, ::GenericObject, file_path)
  end

  ##
  # Process multi-value fields, clean up and convert values if needed
  def process_attrs(attrs)
    attrs.dup.map do |k, v|
      attrs[k] = v.split("|").map { |val| val.strip } if attrs[k].is_a?(String)
    end
  end

  # Ingest object metadata and files
  # @param attrs[Hash]
  # @param user[User] - the depositor
  # @param model[String] - the model name of the object to create
  # @param file_path[String] - the base path to files
  def perform_ingest(attrs, user, model, file_path)
    file_names = attrs[FILE_NAME_KEY]

    # create objcect with workflow
    work = ingest_metadata(attrs, user, model)

    # ingest the files
    ingest_files(work, file_path, file_names, user)

    work
  end

  # Ingest object metadata
  # @param attrs[Hash]
  # @param user[User] - the depositor
  # @param model[String] - the model name of the object to create
  def ingest_metadata(attrs, user, model)
    # create batch upload entry
    batch_upload_entry = BatchUploadEntry.new(batch_upload_id: @batch_record.id,
      raw_metadata: attrs.to_json)
    batch_upload_entry.save

    # Create object
    work = model.new
    attrs.each { |k, v| work.public_send("#{k}=", v) if work.respond_to?(k.to_s) }
    work = Hyrax.persister.save(resource: work)
    # Index the object
    Hyrax.publisher.publish("object.metadata.updated", object: work, user: user)

    # Add to workflow
    workflow_created = Hyrax::Workflow::WorkflowFactory.create(work, {}, user)
    if workflow_created
      # Link batch_upload_entry with Sipity::Entity
      batch_upload_entry.entity_id = Sipity::Entity(work).id
      batch_upload_entry.save
    end

    work
  end

  # Ingest the file(s)
  # @param work[GenericObject]
  # @param file_path[String] - the base path to files
  # @param file_names[Array<String>] - the base path to files
  # @param user[User] - the depositor
  def ingest_files(work, file_path, file_names, user)
    # Upload the file(s)
    if file_names.present?
      file_service = InlineBatchUploadHandler.new(work: work)
      file_service.add(files: build_files(user, file_path, file_names))
      file_service.attach

      Hyrax.persister.save(resource: work)
      # Index the object
      Hyrax.publisher.publish("object.metadata.updated", object: work, user: user)
    end
  end

  ##
  # Build upload file(s)
  # @param user [User] - the depositor
  # @param file_names Array [String] - the filenames
  # @param file_path Array [String] - the parent directory
  # @return Array [BatchUploadFile]
  def build_files(user, file_path, file_names)
    [].tap do |p|
      file_names.each do |f|
        staging_area_file = create_staging_area_file(file_path, f)
        p << BatchUploadFile.new(user: user, file: staging_area_file)
      end
    end
  end

  ##
  # Create staging area file
  # @param path [String] - the parent directory
  # @param filename [String] - the filename
  # @return [StagingAreaFile]
  def create_staging_area_file(path, filename, file_use = :original_file)
    s3_enabled = Rails.application.config.staging_area_s3_enabled
    return StagingAreaFile.new(path: path, filename: filename, file_use: file_use) unless s3_enabled

    s3_handler = Rails.application.config.staging_area_s3_handler
    S3StagingAreaFile.new(path: path, filename: filename, file_use: file_use, s3_handler: s3_handler)
  end

  ##
  # Persist the batch upload record
  # @param permitted [Hash] - the permitted parameters
  # @param source_file [String]- the location of the csv source file.
  def persist_batch_upload_record(permitted:, source_file:)
    source_content = source_file ? File.read(source_file) : ""
    files_location = permitted.key?(:files_location) ? permitted[:files_location] : ""
    batch_id = "b#{Time.now.strftime("%Y%m%d%H%M%S%L")}"
    @batch_record = BatchUpload.new(batch_id: batch_id,
      source_file: source_content, files_path: files_location, user_id: current_user.id)

    @batch_record.save
  end

  ##
  # Create CSV source for files found
  # @param files_path [String] - the path to the files
  # @return [TempFile] for the CSV source file created
  def create_csv_sources_with_files(files_path)
    headers = ["object unique id", "level", "file name", "title"]
    csv_file = Tempfile.new("files-only.csv")
    CSV.open(csv_file, "wb") do |csv|
      csv << headers
      list_files(files_path).each do |f|
        csv << [f, "object", f, f]
      end
    end

    csv_file.path
  end

  ##
  # List file names under a directory
  # @files_path [String] - the directory
  def list_files(files_path)
    s3_enabled = Rails.application.config.staging_area_s3_enabled
    return Rails.application.config.staging_area_s3_handler.get_filenames(files_path) if s3_enabled

    [].tap do |pro|
      Dir.entries(files_path).each do |f|
        pro << f if File.file?("#{files_path}/#{f}")
      end
    end
  end
end
